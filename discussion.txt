// [SECTION] Creating a simple server using ExpressJS Framework

// Use the require() directive to load the express module/package
// It allows us to access the methods and functions that will help us create a server
const express = require("express");

// Create an application using express
// This creates an express application and stores this in a constant called app
// In simple termns, app is now our server
const app = express();

const port = 3000;

// Setup for allowing the server to handle data from requests
// In simple terms, it allows your app to read json data
// Methods used are called middlewares
// Middleware is software that provides common services and capabilities to applications outside of what’s offered by the operating system
// API management is one of the common application of middlewares
app.use(express.json());
// allows your applicxation to read data from forms
// By default, information received from the url can only be received as a string or an array
// By applying the option of "extended:true" this allows us to receive information in other data types such as an object which we will use throughout our application
app.use(express.urlencoded({extended:true}));

// [SECTION] Creating Routes

// GET METHOD
app.get("/", (req, res) => {

	res.send("Hello World");

});

// POST METHOD
app.post("/", (req, res) => {

	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);

});

// Mock Database
let users = [];

// Signup Method
app.post("/signup", (req, res) => {

	if(req.body.username !== "" && req.body.password !== "") {

		users.push(req.body);
		res.send(`User ${req.body.username} is successfully registered!`);

	} else {

		res.send(`Please input BOTH username and password!`);

	}

	console.log(req.body);

});

// Change Password
app.put("/change-password", (req, res) => {

	// create a variable that will store the message to be sent back to the client
	let message;

	for(let user = 0; user < users.length; user++) {

		if(req.body.username == users[user].username) {

			users[user].password = req.body.password;
			message = `User ${req.body.username} has succesfully change their password`;
			break;

		} else {

			message = `User ${req.body.username} does not exist`;
		}
	}

	res.send(message);
	console.log(users);

})

app.listen(port, () => console.log(`Server is listening at localhost:${port}`));